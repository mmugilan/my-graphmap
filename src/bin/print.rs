extern crate graph_map;
use graph_map::GraphMMap;

fn main() {
    // println!("usage: print <source>");

    let filename = std::env::args().skip(1).next().unwrap();
    let n :u32 = std::env::args().skip(2).next().unwrap().parse().unwrap();
    let m :u32= std::env::args().skip(3).next().unwrap().parse().unwrap();
    let graph = GraphMMap::new(&filename);    
    println!("{}", filename);
    println!("{}", graph.nodes());
    graph.print();
    for node in 0 .. graph.nodes() {
        for &edge in graph.edges(node) {
            println!("{}\t{}", node, edge);
        }
    }
}